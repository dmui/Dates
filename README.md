# Dates

#### 介绍
极简的日期时间处理库

#### 解决了什么

1.  **格式化** 日期时间
2. 以 **天** 为单位偏移计算
3. 以 **分钟** 为单位偏移计算
4. 计算两个时间的 **差值** ，例如 **电商的秒杀倒计时场景** 
5. 将时间转换为 **北京时间** ，例如跨境、全球服务场景
6. 将时间转换为指定 **时区** 时间
7. 修复safir浏览器的时间问题



#### 使用说明


```
new Dates().format()； // 2023-05-06 15:27:50
new Dates(1683422870000).format()； // 2023-05-06 15:27:50
new Dates(1683422870000).format('YY-MM-DD')； // 23-05-06

// 日期比较
new Dates().diffWith('2023-5-8 18:00:00') 
// 结果 { "date": 0, "hour": 2, "minutes": 0, "second": 0, "millisecond": 0, "offset": -7200000 }
new Dates('2023-5-8 16:00:00').diffWith('2023-5-8 18:00:00') 
// 结果 { "date": 0, "hour": 2, "minutes": 0, "second": 0, "millisecond": 0, "offset": -7200000 }

// 往后推2天
new Dates().days(-2).format() // 2023-05-04 15:27:50

// 往后推2分钟
new Dates().minutes(-2).format() // 2023-05-04 15:25:50

// 转为北京时间
new Dates().toBeijing().format() // 2023-05-06 15:27:50

// 转为指定时区的时间（如北京时间，则传入8）
new Dates(1683422870000).setUTC(-1).format() // 2023-05-07 00:27:50
```

| api     | 参数 | 说明    | 默认值 |
|---------|----|-------|-----|
| 构造函数    |  value  |   [非必传][string 、number]，可传入时间戳（毫秒）或 字符串时间日期    |  本地时间   |
| format  |  value  | [非必传] 时间格式化 |  'YYYY-MM-DD hh:mm:ss'   |
| diffWith  |  value，unitType  | value[必传]，将要比较的目标日期时间；unitType [非必传]， 比较结果的类型，可传入date、hour、minutes、second、millisecond，不传默认返回Object： _{date：天，hour：时，minutes：分，second：秒，millisecond：毫秒, offset: 时间戳差值}_  |   -  |
| days    |  value  |   [非必传] 偏移天数    |   -  |
| minutes |  value    |  [非必传] 偏移分钟数   |   -  |
| toBeijing|  -  |    转换为北京时间   |   -  |
| setUTC|   value   |  [必传] 转换时间时区，比如，北京（东8区），传入8；夏威夷（西10区），传入 -10。时区查询地址：https://www.beijing-time.org/shiqu/    |  -   |


#### 注意事项
- 实例化时，允许传入指定时间日期，如字符串时间日期（'2022-12-12'、'2022-12-12 12:12:12'），如时间戳（1683422870000）；
- 实例化时，如果传入的 **参数有误时** ，将返回'NaN-NaN-NaN NaN:NaN:NaN'；
- 实例化后，对实例api进行调用时，如days()、minutes()、toBeijing()、setUTC()操作时，都将 **操作同一个时间对象** ，在使用时请注意；