/***
 * 极简的时间日期处理库
 * time：2023-2-2
 * huaicheng151201@163.com
 */
function Dates(value) {
  if (typeof value === 'string' && navigator.userAgent.indexOf("Safari") > -1) {
    value = value.replace(/-/g,"/")
  }
  this.date = value ? new Date(value) : new Date();
}

/**
 * 格式化日期
 * @param {string} value
 */
Dates.prototype.format = function (value = "YYYY-MM-DD hh:mm:ss") {
  const obj = {
    YYYY: this.date.getFullYear(),
    YY: this.date.getFullYear(),
    MM: this.fill(this.date.getMonth() + 1),
    M: this.date.getMonth() + 1,
    DD: this.fill(this.date.getDate()),
    D: this.date.getDate(),
    hh: this.fill(this.date.getHours()),
    h: this.date.getHours(),
    mm: this.fill(this.date.getMinutes()),
    m: this.date.getMinutes(),
    ss: this.fill(this.date.getSeconds()),
    s: this.date.getSeconds(),
  };
  return value
    .replace(/YYYY/, obj.YYYY)
    .replace(/YY/, obj.YY)
    .replace(/MM/, obj.MM)
    .replace(/M/, obj.M)
    .replace(/DD/, obj.DD)
    .replace(/D/, obj.D)
    .replace(/hh/, obj.hh)
    .replace(/h/, obj.h)
    .replace(/mm/, obj.mm)
    .replace(/m/, obj.m)
    .replace(/ss/, obj.ss)
    .replace(/s/, obj.s);
};
/**
 * 偏移天数（>0表示往前推几天，<0表示往后推几天）
 * @param {number} value 
 */
Dates.prototype.days = function (value) {
  if(value){
    const unit = 1000 * 60 * 60 * 24;
    this.date.setTime(this.date.getTime() + value * unit);
  }
  return this;
};
/**
 * 偏移分钟数（>0表示往前推几分钟，<0表示往后推几分钟）
 * @param {number} value 
 */
Dates.prototype.minutes = function (value) {
  if(value){
    const unit = 1000 * 60;
    this.date.setTime(this.date.getTime() + value * unit);
  }
  return this;
};
/**
 * 填补0
 * @param {number} value 
 */
Dates.prototype.fill = function (value) {
  return value < 10 ? "0" + value : value;
};
/**
 * 北京时间
 * @param {number} value 
 */
Dates.prototype.toBeijing = function(){
  this.setUTC(8)
  return this
}
/**
 * 设置时区
 * 比如，北京（东8区），传入8；
 * 比如，夏威夷（西10区），传入 -10）
 * 具体可查询：https://www.beijing-time.org/shiqu/
 * @param {number} value 时区偏移量
 */
Dates.prototype.setUTC = function(value = 0){
  const localTimeZone = this.date.getTimezoneOffset()
  this.minutes(localTimeZone)
  this.minutes(60 * value)
  return this
}
/**
 * 比较两个日期时间
 * @param {number|string} value 时间日期字符串 或 时间戳
 * @param {string} unitType 比较类型（支持天、时、分、秒、毫秒）
 */
Dates.prototype.diffWith = function(value, unitType){
  const time1 = this.date.getTime()
  const time2 = new Date(value).getTime()
  let offset = time2 - time1

  switch(unitType){
    // 毫秒
    case 'millisecond': return offset;
    // 秒
    case 'second': return Math.floor(offset / 1000 );
    // 分
    case 'minutes': return Math.floor(offset / 1000 / 60 );
    // 时
    case 'hour': return Math.floor(offset / 1000 / 60 / 60 );
    // 天
    case 'date': return Math.floor(offset / 1000 / 60 / 60 / 24);
  }
  // 天、时、分、秒、毫秒
  const date = Math.floor(offset / 1000 / 60 / 60 / 24);
  offset -= date * 1000 * 60 * 60 * 24;
  const hour = Math.floor(offset / 1000 / 60 / 60 );
  offset -= hour * 1000 * 60 * 60;
  const minutes = Math.floor(offset / 1000 / 60 );
  offset -= minutes * 1000 * 60;
  const second = Math.floor(offset / 1000 );
  offset -= second * 1000
  const millisecond = offset
  return {
    date,
    hour,
    minutes,
    second,
    millisecond,
    offset: time1 - time2
  }
}